import React from 'react';
import './toDoItem.css'

const toDoItem=props=>{
    return(
        <div className="todo-item">
            <input type="checkbox" className="checkbox"/>
            <p className="description">{props.description}</p>
        </div>
    )
}
export default toDoItem;