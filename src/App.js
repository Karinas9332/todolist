import './App.css';
import ToDoItem from './toDo/toDoItem/toDoItem';
import todosData from './toDo/todosData.js';

function App() {
  const todosItems=todosData.map(item=>{
    return(
  <ToDoItem  
  key={item.id}
  description={item.text}/>
    );
})
return(
    <div className="App">
        {todosItems}
    </div>
)
}
export default App;
