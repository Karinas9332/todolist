const todosData=[
{
    id:1,
    text:"Помыть посуду",
    completed:false
},
{
    id:2,
    text:"Сделать тесты",
    completed:false
},
{
    id:3,
    text:"Приготовить еду",
    completed:false
},
{
    id:4,
    text:"Заказать посылку",
    completed:false
},
{
    id:5,
    text:"Пропылесосить",
    completed:false
}
]
export default todosData;